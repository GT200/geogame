# Geogame

Divers jeu géographique developpés par les élèves de l'ensg. 
chemin: geogame/geo-escape-game/Geo007
Serveur: MAMP à bien configurer pour la base de données (PgSQL)
Base de données: PostgreSQL (PgAdmin), guide d'installation: https://www.enterprisedb.com/downloads/postgres-postgresql-downloads
Connexion à la base de données: 
- créer à la main une base de données pour le jeu (nom libre)
- modifier les paramètres de connect.php: host (localhost si en local), database, user, password
Les tables sont crées automatiquement par le fichier index.php
Consigne du jeu: 
Toutes les instructions sont dans les pop up des marqueurs ou sont en haut de la page.
Remarque: 
Le 007.js est le js initiale du jeu mais on n'y importait directement tous les objets presents dans les tables.
