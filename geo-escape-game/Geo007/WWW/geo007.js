//----------------map-----------------------
//my key acces to my mapbox account for map projetct

L.mapbox.accessToken = 'pk.eyJ1IjoiZ3QyMDAiLCJhIjoiY2t1cmliYTFoMnF5cTJwbzZxZnJ0bjliaiJ9.bUqWJZczXyOGM3VC1nJD-w';

//creating the map from mapbox layer and associate with the élément id map
var map_center_view_coord = [0, 0];
var zoom_limit = {minZoom:0, maxZoom:20};
var map = L.mapbox.map("map",undefined, zoom_limit).setView(map_center_view_coord,minZoom = 2.4, maxZoom = 2.4).addLayer(L.mapbox.styleLayer('mapbox://styles/mapbox/streets-v10'));

//-------------------------------Variables-------------------------------------------
//html
let support_agent_textarea= document.getElementById("support-agent-textarea");
const audioDrNo = document.getElementById("audioDrNo");
const audioAirplane = document.getElementById("audioAirplane");
var chrono = document.getElementById('chrono-container');
var user = document.getElementById('user-info');

audioDrNo.play();
let support_agent;
let support_agent_info_size;
//DB
let DB_json;
let curent_user_object;
let user_rank;
let user_name;
let user_score;
let user_time;
//map
var markerZoomGroup1 = new L.FeatureGroup();
var markerZoomGroup2 = new L.FeatureGroup();
var markerZoomGroup3 = new L.FeatureGroup();
var markerZoomGroup4 = new L.FeatureGroup();
var markerZoomGroup5 = new L.FeatureGroup();
let envelop;
let BondMovie1RealName;
let bond0;
let gun;
let bond0Circle;
let airplaneCircle;
//other
let writerSpeed= 50;
var dsec = 0;
var dmin = 0;
var dhr = 0;

//-------------------------------------------------------------------

function typeWriter(element, text, speed1) {
  element.innerHTML += text.charAt(0);
  var currentScrollHeight = element.scrollHeight;
  element.scrollTop = (element.scrollTop + 100); //set the scroll height to scroll by here.
  text = text.substring(1, text.length);
  //console.log(text.length)
  if (text.length == 0){
    support_agent_info_size = text.length;
    return support_agent_info_size;
  }
  else{
  setTimeout(typeWriter, speed1, element, text, speed1);
  }
}

function infoAndSizeHandler(layer, zoom){
  //console.log(layer.getlatLng());
  if (support_agent_info_size == 0){
    map.on('zoomend', function() {
      if (map.getZoom() <zoom){
        map.removeLayer(layer);
      }
      else {
        map.addLayer(layer);
          }
    });

    }
  else{
    setTimeout(infoAndSizeHandler, writerSpeed,layer, zoom);
  }
}

function createCustomedMarker(object){
  let popupContent = object["text_content"];
  let markerIcon = L.icon({iconUrl: object["path"], iconSize: [100, 100], popupAnchor:  [0, -50]});
  let popupOptions = {'maxWidth': '500','className' : 'custom'};
  let customedMarker= L.marker([object["lat"], object["lng"]], {icon: markerIcon}).bindPopup(popupContent,popupOptions);
  return (customedMarker);
}

//------------------------------asynch ajax-------------------------------------------

function ajaxSuportAgent(object_name){
  let data = new FormData();
  data.append('name', object_name);
  fetch('geo007.php', {
    method: 'post',
    body: data
  }).then(result => result.json()).then( result => {
  DB_json = result;
  console.log(DB_json);
  curent_object = DB_json[0];
  let support_agent_info_0 = curent_object["text_content"];
  support_agent_textarea.innerHTML = "";
  support_agent = typeWriter(support_agent_textarea, support_agent_info_0, writerSpeed);
})
}

ajaxSuportAgent("support_agent_info_0");

function ajaxSympleObject0(object_name, layerGroup){
  let data = new FormData();
  data.append('name', object_name);
  fetch('geo007.php', {
    method: 'post',
    body: data
  }).then(result => result.json()).then( result => {
  DB_json = result;
  console.log(DB_json);
  curent_object = DB_json[0];
  let marker = createCustomedMarker(curent_object);
  layerGroup.addLayer(marker)
  });
}


function ajaxSympleObject(object_name, layerGroup, event, event_function, isDraggableLocation, isDraggableRadius){
  let data = new FormData();
  data.append('name', object_name);
  fetch('geo007.php', {
    method: 'post',
    body: data
  }).then(result => result.json()).then( result => {
  DB_json = result;
  console.log(DB_json);
  curent_object = DB_json[0];
  //let marker = createCustomedMarker(curent_object);

  if (event == 'drag'){
    //map.removeLayer(layerToAdd);
    let marker = createCustomedMarker(curent_object);
    marker.addTo(map);
    let Circle = L.circle([isDraggableLocation.lat, isDraggableLocation.lng], {
      color: 'green',
      fillColor: '#f03',
      fillOpacity: 0.5,
      radius: isDraggableRadius
    });
    layerGroup.addLayer(Circle)
    infoAndSizeHandler(layerGroup, curent_object["zoom"]);


    marker.on(event, function (){
      distance_from_centerPoint = marker.getLatLng().distanceTo(isDraggableLocation);
      // See if meters is within radius
      if (distance_from_centerPoint <= isDraggableRadius) {
        map.removeLayer(marker);

        map.eachLayer(function (Alayer){ if (Alayer instanceof L.FeatureGroup){ if (Alayer == layerGroup){Alayer.eachLayer(function (layer){Alayer.removeLayer(layer)})} }});
      }
    });


    //draggableSizeHandler(marker, curent_object["zoom"]);
    marker.dragging.enable();
  }
  else{
    let marker = createCustomedMarker(curent_object);
    marker.on(event, event_function);
    layerGroup.addLayer(marker);
    infoAndSizeHandler(layerGroup, curent_object["zoom"]);
  }

  })
};

function noEvent(){
  console.log("no event");
}
function noMarkerToAdd(){
  console.log("no event");
}

function checkPass(e) {
  console.log("CCCCLLLLIIIICCCCCCCCKKK");
  var mdp_element = document.getElementById("mdp0");
  mdp_element.addEventListener('change', function(){console.log(mdp_element.value);
  if (mdp_element.value.toLowerCase() == "sean connery"){
    support_agent_textarea.innerHTML = "";
    ajaxSuportAgent("support_agent_info_1");
    ajaxSympleObject("bond0",markerZoomGroup1, 'click', bon0Event,L.latLng(0, 0), 0);
  }
  else{
    support_agent_textarea.innerHTML = "";
    ajaxSuportAgent("support_agent_info_2");
    ajaxSympleObject("BondMovie1RealName",markerZoomGroup1, 'click', noEvent,L.latLng(0, 0), 0);
  }
  })
}

function bon0Event(e) {
  console.log("CCCCLLLLIIIICCCCCCCCKKK");
  ajaxSympleObject("mi6", markerZoomGroup1, 'drag', noEvent, L.latLng(51.50348095987888, -0.1499355430760572), 500);
  ajaxSympleObject("bond0_1", markerZoomGroup2, 'drag', noEvent, L.latLng(51.51217491052512, 0.0540497243448876), 500);
  ajaxSympleObject("london_city_aiport", markerZoomGroup3, 'drag', noEvent, L.latLng(17.937585783326213, -76.77902328129507), 200);
  ajaxSympleObject("kingston_airport", markerZoomGroup4, 'click', noEvent, L.latLng(0, 0), 0);
  ajaxSympleObject("kingston_airport_trapper", markerZoomGroup5, 'click', endUnfinisedGame, L.latLng(0, 0), 0);
}

function endUnfinisedGame(e){
  ajaxSuportAgent("support_agent_info_3");
  ajaxSympleObject("bond0_2", markerZoomGroup4, 'drag', noEvent, L.latLng(51.50137065829317, -0.1418980483055628), 200, noMarkerToAdd);
  ajaxSympleObject("ElisabethQeen", markerZoomGroup5, 'click', update_user_time, L.latLng(0,0), 0, noMarkerToAdd);
}

// ajaxSympleObject("envelop", 'click', checkPass, L.latLng(0,0), 0, noMarkerToAdd);

function ajax(object_name, event, event_function){

  let data = new FormData();

  data.append('name', object_name);

  fetch('geo007.php', {

    method: 'post',

    body: data

  }).then(result => result.json()).then( result => {

  DB_json = result;

  console.log(DB_json);

  curent_object = DB_json[0];
  curent_object_content = curent_object["text_content"];
  user_name = curent_object_content.split(' ')[5];
  user.innerText = user_name;
  console.log(user_name);

  let  marker =createCustomedMarker(curent_object);

  markerZoomGroup1.addLayer(marker);

  infoAndSizeHandler( markerZoomGroup1, curent_object["zoom"]);

  marker.on(event, event_function);

  //console.log(mdp_element);

  })

}

ajax("envelop", 'click', checkPass);

function chronometre() {
  dsec++;
	 if (dsec >= 60){
     dsec=0;
     dmin++;
   }
   if (dmin>=60) {
     dmin=0;
     dhr++;
   }
   chrono.innerText = (dhr > 9 ? dhr : "0" + dhr) + ":" + (dmin > 9 ? dmin : "0" + dmin) + ":" + (dsec > 9 ? dsec : "0" + dsec) ;
   setTimeout(chronometre, 1000);
}

chronometre();

function secondes(temps){
  return temps.substring(0,2)*3600 + temps.substring(3,5)*60 + temps.substring(6,8);
}

function update_user_time(e){
  console.log("TIME");
  let data = new FormData();

  data.append('user_name', user.innerText);
  data.append('time', chrono.innerText);
  data.append('timesec', secondes(chrono.innerText));

  fetch('geo007.php', {

    method: 'post',

    body: data

  });
}
