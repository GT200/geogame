//----------------map-----------------------
//my key acces to my mapbox account for map projetct 

L.mapbox.accessToken = 'pk.eyJ1IjoiZ3QyMDAiLCJhIjoiY2t1cmliYTFoMnF5cTJwbzZxZnJ0bjliaiJ9.bUqWJZczXyOGM3VC1nJD-w';

//creating the map from mapbox layer and associate with the élément id map
var map_center_view_coord = [0, 0];
var zoom_limit = {minZoom:0, maxZoom:20};
var map = L.mapbox.map("map",undefined, zoom_limit).setView(map_center_view_coord,minZoom = 2.4, maxZoom = 2.4).addLayer(L.mapbox.styleLayer('mapbox://styles/mapbox/streets-v10'));

//-------------------------------Variables-------------------------------------------
//html
let support_agent_textarea= document.getElementById("support-agent-textarea");
const audioDrNo = document.getElementById("audioDrNo");
const audioAirplane = document.getElementById("audioAirplane");
audioDrNo.play();
let support_agent;
let support_agent_info_size;
//DB
let DB_json;
let curent_user_object;
let user_rank;
let user_name;
let user_score;
let user_time;
//map
var markerZoom7Group = new L.FeatureGroup();
let envelop;
let BondMovie1RealName;
let bond0;
let gun;
let bond0Circle;
let airplaneCircle;
//other
let writerSpeed= 10;

//-------------------------------------------------------------------

function typeWriter(element, text, speed1) {
  element.innerHTML += text.charAt(0);
  var currentScrollHeight = element.scrollHeight;
  element.scrollTop = (element.scrollTop + 100); //set the scroll height to scroll by here.
  text = text.substring(1, text.length);
  //console.log(text.length)
  if (text.length == 0){
    support_agent_info_size = text.length;
    return support_agent_info_size;
  }
  else{
  setTimeout(typeWriter, speed1, element, text, speed1);
  }
}

//------------------------------asynch ajax-------------------------------------------
function ajax(){
  fetch('geo007.php').then(result => result.json()).then( result => { 
  DB_json = result;
  console.log(DB_json);
  //curent user info
  let lastKey = Object.keys(DB_json).length;
  curent_user_object = DB_json[lastKey - 1];
  user_rank = curent_user_object["rank"];
  user_name = curent_user_object["name"];
  user_score = curent_user_object["score"];
  user_time = curent_user_object["time"];
  //Store objects as associative array using name attibute as key
  let DB_Assoc_array = {}; 
  DB_json.forEach( (elem) => { 
    DB_Assoc_array[elem["name"]] = elem;
  });
  console.log(DB_Assoc_array);
  let support_agent_info_0 = DB_Assoc_array["support_agent_info_0"]["text_content"];
  support_agent = typeWriter(support_agent_textarea, support_agent_info_0, writerSpeed);

  function createCustomedMarker(object_name){
    let object = DB_Assoc_array[object_name];
    let popupContent = object["text_content"];
    let markerIcon = L.icon({iconUrl: object["path"], iconSize: [100, 100], popupAnchor:  [0, -50]});    
    let popupOptions = {'maxWidth': '500','className' : 'custom'};
    let customedMarker= L.marker([object["lat"], object["lng"]], {icon: markerIcon}).bindPopup(popupContent,popupOptions);
    return (customedMarker);
  }
  let envelop = createCustomedMarker("envelop");
  let BondMovie1RealName= createCustomedMarker("BondMovie1RealName");
  let bond0= createCustomedMarker("bond0");
  let gun= createCustomedMarker("mi6");
  let airplane= createCustomedMarker("london_city_aiport");
  let kingston_airport= createCustomedMarker("kingston_airport");
  let kingston_airport_trap= createCustomedMarker("kingston_airport_trap");
  let Spanish_town = createCustomedMarker("Spanish_town");
  
  let marker7Group1 = [envelop, BondMovie1RealName, ]


  let envelopZoom=11;
  let BondMovie1RealNameZoom=11;
  let bond0Zoom=13;
  let gunZoom=15;
  let airplaneZoom=5;
  let kingston_airportZoom=11;

  //markerZoom7Group.addLayer(envelop);

  envelop.on('click', checkPass);
  bond0.on('click', bon0Event);
  gun.on('drag', gunDragEvent);
  bond0.on('drag', bond0DragEvent);
  airplane.on('drag', airplaineDragEvent);


  function infoAndSizeHandler(layer, zoom){
    if (support_agent_info_size == 0){
      map.on('zoomend', function() {
        if (map.getZoom() <zoom){
          map.removeLayer(layer);
        }
        else {
          map.addLayer(layer);
            }
      });
      
      }
    else{
      setTimeout(infoAndSizeHandler, writerSpeed,layer, zoom);
    }
  }

  function draggableSizeHandler(marker, zoom){
      map.on('zoomend', function() {
        if (map.getZoom() <zoom){
          marker.addTo(map);
        }
        else {
          map.addLayer(marker);
            }
      });
  }
  


  infoAndSizeHandler(envelop, envelopZoom);

  function checkPass(e) {
    console.log("CCCCLLLLIIIICCCCCCCCKKK");
    //var popup = e.target.getPopup();
    //var content = popup.getElement();
    var mdp_element = document.getElementById("mdp0");
    mdp_element.addEventListener('change', function(){console.log(mdp_element.value); 
    if (mdp_element.value.toLowerCase() == "sean connery"){
      support_agent_textarea.innerHTML = "";
      let support_agent_info_1 = DB_Assoc_array["support_agent_info_1"]["text_content"];
      console.log(support_agent_info_1);
      support_agent = typeWriter(support_agent_textarea, support_agent_info_1, writerSpeed);
      //markerZoom7Group.addLayer(bond0);
      infoAndSizeHandler(bond0, bond0Zoom);
    }
    else{
      support_agent_textarea.innerHTML = "";
      let support_agent_info_2 = DB_Assoc_array["support_agent_info_2"]["text_content"];
      console.log(support_agent_info_2);
      support_agent = typeWriter(support_agent_textarea, support_agent_info_2, writerSpeed);
      //markerZoom7Group.addLayer(BondMovie1RealName)
      infoAndSizeHandler(BondMovie1RealName, BondMovie1RealNameZoom);
    }
  
    })
    //console.log(mdp_element);
  }

  function bon0Event(e) {
    console.log("CCCCLLLLIIIICCCCCCCCKKK");
    gun.addTo(map);
    //draggableSizeHandler(gun, gunZoom);
    infoAndSizeHandler(gun, gunZoom);
    gun.dragging.enable();
    bond0Circle = L.circle([bond0.getLatLng().lat, bond0.getLatLng().lng], {
      color: 'red',
      fillColor: '#f03',
      fillOpacity: 0.5,
      radius: 500
    });
    infoAndSizeHandler(bond0Circle, bond0Zoom);

  }

  function gunDragEvent(e) {
    console.log("DRAAAAAAGFGGGGGGGGGGGGG");
    console.log(gun.getLatLng());
    console.log(gun.getLatLng().lat, gun.getLatLng().lng);
    let theCenterPt = bond0Circle.getLatLng();
    let theRadius = bond0Circle.getRadius();
    console.log(theRadius.lat, theCenterPt);
    distance_from_centerPoint = gun.getLatLng().distanceTo(theCenterPt);
    // See if meters is within radius
    if (distance_from_centerPoint <= 0.8*theRadius) {
      map.removeLayer(gun);
      infoAndSizeHandler(gun, 20);
      bond0.getPopup().setContent("Thanks you agent " + user_name +". Now, take me to london city airport. Here is the coordinate: 51.51217491052512, 0.0540497243448876");
      bond0.getPopup().update();
      airplane.addTo(map);
      infoAndSizeHandler(airplane, airplaneZoom);
      map.removeLayer(bond0Circle);
      infoAndSizeHandler(bond0Circle, 20);
      bond0.dragging.enable();

      airplaneCircle = L.circle([airplane.getLatLng().lat, airplane.getLatLng().lng], {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.5,
        radius: 200
      });
      infoAndSizeHandler(airplaneCircle, airplaneZoom);

    }
  }

  function bond0DragEvent(e) {
    console.log("DRAAAAAAGFGGGGGGGGGGGGG");
    console.log(bond0.getLatLng());
    console.log(bond0.getLatLng().lat, bond0.getLatLng().lng);
    let airplaneCenterPt = airplaneCircle.getLatLng();
    let theRadius = airplaneCircle.getRadius();
    console.log(theRadius.lat, airplaneCenterPt);
    distance_from_centerPoint = bond0.getLatLng().distanceTo(airplaneCenterPt);
    // See if meters is within radius
    if (distance_from_centerPoint <= theRadius) {
      map.removeLayer(markerZoom7Group);
      //audioDrNo.pause();
      audioAirplane.play();
      airplane.dragging.enable();
      map.removeLayer(airplaneCircle);
      infoAndSizeHandler(airplaneCircle, 20);
      map.removeLayer(bond0);
      infoAndSizeHandler(bond0, 20);
      //kingston_airport.addTo(map);
      infoAndSizeHandler(kingston_airport, kingston_airportZoom);
      }
      spanishTownCircle = L.circle([airplane.getLatLng().lat, airplane.getLatLng().lng], {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.5,
        radius: 2000
      });
      distance_from_centerPoint = bond0.getLatLng().distanceTo(Spanish_town.getLatLng());
      theRadius = 2000;
      if (distance_from_centerPoint <= 0.7*theRadius) {
        map.removeLayer(bond0);
        location.href='index.html';
        
      }


  }

  function airplaineDragEvent(e) {
    console.log("DRAAAAAAGFGGGGGGGGGGGGG");
    console.log(airplane.getLatLng());
    console.log(airplane.getLatLng().lat, airplane.getLatLng().lng);
    distance_from_centerPoint = airplane.getLatLng().distanceTo(kingston_airport.getLatLng());
    theRadius = 1000;
    // See if meters is within radius
    if (distance_from_centerPoint <= theRadius) {
      map.addLayer(markerZoom7Group);
      bond0.setLatLng(kingston_airport.getLatLng()).update();
      bond0.getPopup().setContent("Agent " + user_name +"! We arrived. Look around for our a welcomer agent");
      bond0.getPopup().update();
      infoAndSizeHandler(bond0, bond0Zoom);
      infoAndSizeHandler(airplane, 20);
      //airplane.dragging.disable();
      airplane.setLatLng(kingston_airport.getLatLng()).update();
      infoAndSizeHandler(kingston_airport_trap, 12);
      audioAirplane.pause();

      infoAndSizeHandler(envelop, 20);
      infoAndSizeHandler(BondMovie1RealName, 20);

      infoAndSizeHandler(Spanish_town, 10);
    }
  }


  //DB_json.forEach( (elem) => { console.log(elem)});
  });
}
ajax();


