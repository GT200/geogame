<?php
    include("connect.php");

    //page d'accueil
    if (isset($_POST["user"]) && !empty($_POST["user"])){
        $user = $_POST["user"];

        $create_user_tb_request = "CREATE TABLE IF NOT EXISTS user_tb (rank integer, name text,score integer,time text)";
        $result_create_user_tb_request = pg_query($connection, $create_user_tb_request);

        $create_tool_tb_request = "CREATE TABLE IF NOT EXISTS object_tb (id integer, name text,lat numeric,lng numeric, text_content text, zoom numeric, visibility text, path text)";
        $result_create_tool_tb_request = pg_query($connection, $create_tool_tb_request);

        $drop_tool_tb_request= "DELETE FROM object_tb";
        $result_drop_tool_tb_request = pg_query($connection, $drop_tool_tb_request);

        $add_object_request = "INSERT INTO object_tb VALUES
        ('0', 'support_agent_info_0', '-1000', '-1000', 'Hi agent $user . Welcome to this geo007 escape game. As a new recuit, you will work along side 007 agent as his pupil to gain experience through out numerous missions around the world... About your first mission: One of our agent (agent 999) have left you a package in the capital of Jamaica. Go find it for further instruction ', '14', 'true', 'img/env.png'),

        ('1', 'envelop', '18.013815482231767', '-76.77907078270987' ,'<div id=''JS''><img src=''img/John_Strangways.jpg'' alt=''john_Strangways''/><p>Hello agent $user , this letter is very confidential and may only be opened by you. To make sure that it is you, enter the real name of agent 007 in the Dr.No movie. If you don''t know the code, go get it at Goldeneye hotel here in Jamaica. Use google map research if needed.</p> <input type = ''text'' name=''mdp'' id=''mdp0''></div>', '10', 'true', 'img/env.png'),

        ('2', 'support_agent_info_1', '-1000', '-1000' ,'Good job agent $user, Your identity has been succesfuly verified. Here is the report inside your letter: our fellow Agent, John Strangways, has been murdered. You will work with agent 007 to investigate his disappearance which may be connected with some recent problems the CIA and NASA have been having with a moon rocket. 007 is waiting for you in London. Don''t be late.', '14', 'false', 'img/env.png'),

        ('3', 'support_agent_info_2', '-1000', '-1000' ,'I am very desappointed agent $user. You really don''t know the name of your future mentor. Well, I expected that and I will gave you the answer but not for free. You will have to find it at this location in Jamaica: the GoldenEye hotel, to the north of jamaica, East of Port Maria', '14', 'false', 'img/env.png'),

        ('4', 'BondMovie1RealName', '18.41042698314854', '-76.94364933200396' ,'<img src=''img/bondDrNo.png'' alt=''Sean Connery''/><p>Sean Connery as James Bond, a British MI6 agent, codename 007.</p>',  '14', 'false', 'img/bondDrNo.png'),

        ('5', 'bond0', '51.50348095987888', '-0.1499355430760572' ,'You are finaly here agent $user. I have been informed of your arrival. Before flying to jamaica, I would like you to take my new fire gun at MI6 headquarters in central London. Follow the road A202 to the south until you cross the river Thames', '14', 'false', 'img/bond0.png'),

        ('6', 'bond0_1', '51.50348095987888', '-0.1499355430760572' ,'Thank you agent $user. Now, take me to London City Airport. Here is the coordinates if needed: 51.51217491052512, 0.0540497243448876', '14', 'false', 'img/bond0.png'),

        ('7', 'bond0_2', '17.938585783326213', '-76.77902328129507' ,'Agent $user, We arrived! Look around for our driver', '14', 'false', 'img/bond0.png'),


        ('8', 'mi6', '51.48814335353215', '-0.12075231312965831' ,'gunDrno', '14', 'false','img/gunDrno.png'),

        ('9', 'london_city_aiport', '51.51217491052512', '0.0540497243448876' ,'Flight from London to Kingston', '11', 'false', 'img/airplane.png'),

        ('10', 'kingston_airport', '17.937585783326213', '-76.77902328129507' ,'Kingston airport', '12', 'false', 'img/kingston_airport.png'),

        ('11', 'kingston_airport_trapper', '17.941950593745013', '-76.76773868394713' ,'Welcome Mister Bond, Unfortunately you have to go back to London. Your GameMaster did not finish the game in time and as consequence, You have to face the Queen at Buckingham Palace to get your punishment','14', 'false', 'img/traper.png'),

        ('12', 'support_agent_info_3', '0', '0' ,'To finish the game dear agent $user, drag 007 to  Buckingham Palace. Thank you for your participation. See you soon','14', 'false', 'img/traper.png'),

        ('13', 'ElisabethQeen', '51.50137065829317', '-0.1418980483055628' ,'Me the queen of london, banish from our kingdom. You are unworthy of our intelligence agent. A DIOS! GAME OVER <a href=''index.html''>Check Home Page for Hall OF Fame!</a> ', '14', 'false', 'img/UKQeenRunningLate.png')";

        $result_add_object_request = pg_query($connection, $add_object_request);

        $user_not_in_tb = true;
        $select_user_tb_request = "SELECT * FROM user_tb";
        if ($result_select_user_tb_request = pg_query($connection, $select_user_tb_request)) {
            while ($row = pg_fetch_assoc($result_select_user_tb_request)) {
              if ($row["name"]==$user){
                $user_not_in_tb=false;
              }
            }
        if ($user_not_in_tb){
          $add_user_request = "INSERT INTO user_tb VALUES ('-1', '$user' ,'0' ,'00:00:00')";
          $result_add_user_request = pg_query($connection, $add_user_request);
        }
      }

    }
    //HOF
    else{
      $create_user_tb_request = "CREATE TABLE IF NOT EXISTS user_tb (rank integer, name text,score integer,time text,time_sec numeric)";
      $result_create_user_tb_request = pg_query($connection, $create_user_tb_request);
      $BD=[];
      $select_user_tb_request = "SELECT rank, name, score, time FROM user_tb ORDER BY time_sec LIMIT 10";
      if ($result_select_user_tb_request = pg_query($connection, $select_user_tb_request)) {
        while ($row = pg_fetch_assoc($result_select_user_tb_request)) {
          $BD[] = $row;
        }
        if(sizeof($BD)==0){
          $BD[]=['rank' => '-1', 'name' => 'test', 'score' => '-1', 'time'=>'00:00:00'];
          $BD[]=['rank' => '0', 'name' => 'test2', 'score' => '0', 'time'=>'00:00:00'];
          //$BD=["-----------------------------"];
          echo json_encode($BD);
        }
        else{
          echo json_encode($BD);
        }
      }
    }
?>
